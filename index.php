<?php

$get = $_GET;
// $get['debug'] = 1; // Pour forcer la page à se mettre en mode débogague, c'est-à-dire afficher le contenu de la variable $debug
// $get['force'] = 1; // Pour forcer la page à se mettre en mode force, c'est-à-dire désactiver les pattern, oninput et required

// Autoload des classes
spl_autoload_register(function($className) {
    include_once("src/{$className}.php");
});

// Création du formulaire d'après un fichier JSON
$form = Form::fromJson(file_get_contents('data/main-form.json'));

// Test des données envoyées depuis le formulaire et modification des valeurs du formulaire
// L'affichage ce faisant après, ce sont bien les valeurs de $_POST qui seront affichées, si disponibles
$validForm = false;
if ($_SERVER['REQUEST_METHOD'] == 'POST')
    $validForm = $form->setValues($_POST);

// Variable de débogague
$debug = $form;

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>Formulaire</title>

        <link rel="stylesheet" href="style/water.min.css" />
        <link rel="stylesheet" href="style/main.css" />

        <script src="https://cdnjs.cloudflare.com/ajax/libs/xregexp/3.2.0/xregexp-all.min.js" integrity="sha512-EoQC7ZpFBwvZEbXQeelA6VpL75iUn484CsQX3qqOa27UmBMdFxsQ8ZwX1v0437BOND6fcuH+t/NL1DeQGKcyiw==" crossorigin="anonymous"></script>
        <script src="scripts/main-form-validation.js"></script>

        <?php if (isset($get['debug'])): ?>
            <style>
            body {
                grid-template-areas:
                    "debug debug"
                    "header header"
                    "main aside";
            }

            #debug {
                grid-area: debug;
            }
            </style>

            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.2.0/styles/atelier-lakeside-dark.min.css" integrity="sha512-pskqEMlV3z0PgF904rj74vLCa7S4KO/Nq6toR1LQnMIGSdniVJdBgNtLvO+QgiRorVvsIQh3VQQ6pSqN/e4FnQ==" crossorigin="anonymous" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.2.0/styles/atelier-lakeside-light.min.css" integrity="sha512-5zOhaMShQk11ZSj3JTXlH/jte52IxFM+4Dw2UpDpD8psZ0UL5wXGC/pYqDp3yQrMFc3thI5rTVtznc55kZTTSA==" crossorigin="anonymous" media="(prefers-color-scheme: light)" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.2.0/highlight.min.js" integrity="sha512-TDKKr+IvoqZnPzc3l35hdjpHD0m+b2EC2SrLEgKDRWpxf2rFCxemkgvJ5kfU48ip+Y+m2XVKyOCD85ybtlZDmw==" crossorigin="anonymous"></script>
            <script>hljs.initHighlightingOnLoad();</script>
        <?php endif; ?>

        <?php if (isset($get['force'])): ?>
            <script>
            document.addEventListener("DOMContentLoaded", () => {
                const fields = document.querySelectorAll("form[name='main-form'] input");

                fields.forEach(field => {
                    field.pattern = ".*";
                    field.removeAttribute("oninput");
                    field.removeAttribute("required");
                    field.setCustomValidity("");
                });
            });
            </script>
        <?php endif; ?>
    </head>

    <body>
        <?php if (isset($get['debug'])): ?>
            <details id="debug" <?php echo (in_array($get['debug'], ['open', 'true', '1'])) ? 'open' : ''; ?>>
                <summary>debug</summary>
                <pre><code style="font-size: 12px"><?php var_dump($debug); ?></code></pre>
            </details>
        <?php endif; ?>

        <header id="header">
            <h2>Fondamentaux du web</h2>
            <?php

            // Affichage de la réponse de l'envoie du formulaire
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                if ($validForm)
                    echo '<p class="valid">Votre réponse est correcte, mais n\'a pas été enregistrée pour autant</p>';
                else
                    echo '<p class="invalid">Un champ du formulaire est invalide<br>Veuillez le modifier et ré-envoyer votre réponse</p>';
            }

            ?>
        </header>

        <main>
            <article>
                <header>
                    <h1>Article bien nommé</h1>
                </header>
                <p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>
                <h2>Header Level 2</h2>
                <ol>
                   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                   <li>Aliquam tincidunt mauris eu risus.</li>
                </ol>
                <blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>
                <h3>Header Level 3</h3>
                <ul>
                   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                   <li>Aliquam tincidunt mauris eu risus.</li>
                </ul>
                <pre><code><!--
-->#header h1 a {
    display: block;
    width: 300px;
    height: 80px;
}<!--
                --></code></pre>
            </article>
        </main>

        <aside id="aside">
            <header>
                <h2>Laisser un avis</h2>
            </header>
            <?php $form->display(); // Affichage du formulaire ?>
        </aside>
    </body>
</html>