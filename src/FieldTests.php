<?php

// Classe qui contient les tests customs en PHP pour les champs
// Il n'est pas nécessaire de les mettre comme ça, tout ce qu'il faut c'est mettre dans le JSON le nom d'une fonction qui peut-être appelée depuis Form.php, j'ai simplement décidé de mettre ça dans ce fichier comme j'ai décidé de mettre les tests JS dans main-form-validation.js
// Les tests sont les mêmes qu'en JS, se référer aux commentaires de main-form-validation.js
class FieldTests {

    static private function isValidName($name) {
        return preg_match("/^(?:\pL[\r\n\t\f\v ]*[.,'-]?[\r\n\t\f\v ]*)+$/", $name);
    }

    static public function nom($value) {
        return self::isValidName($value);
    }

    static public function prenoms($value) {
        return empty($value) || self::isValidName($value);
    }

    static public function codePostalResidence($value) {
        return count(json_decode(file_get_contents("https://geo.api.gouv.fr/communes?codePostal=$value")));
    }

    static public function dateNaissance($value) {
        return strtotime($value) <= date('Y-m-d');
    }

}

?>