<?php

// Classe générique d'utilitaires
class Util {

    const SLUG_REGEX = '/^[a-zA-Z][\w-]*$/';

    // Fonction pour générer des exceptions d'arguments
    // C'était simplement pour être facilement maintenable, en réalité elle pose un problème puisque rajoute un appel dans la pile d'exceptions, rendant le débogage plus compliqué pour quelqu'un qui ne connait pas le code
    public static function throwInvalidArgumentException($argumentName, $argumentValue, $reason) {
        $value = '';
        try {
            $value = (string)$argumentValue;
        } catch (Error $e) {
            try {
                $value = get_class($argumentValue);
            } catch (Exception $f) {
                $value = 'wrong value';
            }
        }
        throw new InvalidArgumentException("[$argumentName] $reason (`$value` found)");
    }

}

?>